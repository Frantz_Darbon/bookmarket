package net.tncy.fda.bm.services;

import net.tncy.fda.bm.Book;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

public class BookService {

    public void fakeMethode() {

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Book book = new Book("Mon isbn");

        Set<ConstraintViolation<Book>> violationSet = validator.validate(book);

        for (ConstraintViolation<Book> violation : violationSet) {
            System.out.println(violation.getMessage());
        }
    }
}
